# Avaliação intermediária

#### Introdução

Ao ser ligado, o LCD do OLED, mostrará a hora atual definida no main.c. Toda vez, que for a hora atual, um "H" começa o texto, quando se trata do despertador/alarme um "A" inicia o texto.

#### Diagrama de blocos

O diagrama de blocos abaixo representa como o circuito foi montado.
![alt text](Diagrama_blocos_PI.jpeg)


#### Funcionamento da placa
O funcionamento está descrito abaixo:

Quando o botão da própria placa é acionado, o LCD mostra a hora atual, caso já esteja mostrando a hora atual, é alterado para o horário marcado para o despertador e vice e versa.
![alt text](Hora.jpeg)

Os dois primeiros botões do OLED srvem para alterar o horário do alarme. Sendo o primeiro botão responsável por diminuir os minutos, já o segundo botão por aumentar a hora do alarme.
![alt text](Alarme.jpeg)


Por fim, o último botão do OLED é responsável por desligar o alarme. Caso um alarme esteja tocando ele desliga o alarme, mas também pode desligar um alarme, antes mesmo dele tocar. Ou seja, ele tem a função de deixar o alarme offline

![alt text](Offline.jpeg)
