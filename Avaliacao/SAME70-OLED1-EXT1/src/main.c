/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>

#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"


/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      45
#define SECOND      0

#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   8
#define LED_PIN_MASK   (1<<LED_PIN)

#define BUT_PIO_ID			  ID_PIOA
#define BUT_PIO				  PIOA
#define BUT_PIN				  11
#define BUT_PIN_MASK		  (1 << BUT_PIN)
#define BUT_DEBOUNCING_VALUE  79

#define BUT1_PIO_ID			  ID_PIOD
#define BUT1_PIO		      PIOD
#define BUT1_PIN			  28
#define BUT1_PIN_MASK		  (1 << BUT1_PIN)
#define BUT1_DEBOUNCING_VALUE  79

#define BUT2_PIO_ID			  ID_PIOC
#define BUT2_PIO			  PIOC
#define BUT2_PIN			  31
#define BUT2_PIN_MASK		  (1 << BUT2_PIN)
#define BUT2_DEBOUNCING_VALUE  79

#define BUT3_PIO_ID			  ID_PIOA
#define BUT3_PIO			  PIOA
#define BUT3_PIN			  19
#define BUT3_PIN_MASK		  (1 << BUT3_PIN)
#define BUT3_DEBOUNCING_VALUE  79



/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

uint32_t minuto=0;
uint32_t alarmeminuto = 45;

volatile uint8_t flag_led0 = 0;
volatile uint8_t flag_but = 1;
volatile uint8_t libera_ier = 0;

volatile uint8_t flag_main = 0;
volatile uint8_t flag_menos = 0;
volatile uint8_t flag_mais = 0;
volatile uint8_t flag_desativa = 0;
volatile uint8_t flag_tela = 0;




/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void BUT_init(void);
void pin_toggle(Pio *pio, uint32_t mask);
void RTC_init(void);
void LED_init(int estado);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

static void Button0_Handler(uint32_t id, uint32_t mask){
	flag_main = 1;
	flag_tela = !flag_tela;
	
	
}

static void Button1_Handler(uint32_t id, uint32_t mask){
	if(libera_ier) {
		flag_menos=1;
		flag_tela = 0;
	}
}

static void Button2_Handler(uint32_t id, uint32_t mask){
	if(libera_ier) {
		flag_mais=1;
		flag_tela = 0;
		
	}
		
}
static void Button3_Handler(uint32_t id, uint32_t mask){
	flag_desativa = 1;
	flag_tela = 0;
}

void TC1_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_led0)
		pin_toggle(LED_PIO, LED_PIN_MASK);
}

void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		
		uint32_t h,m,s;
		rtc_get_time(RTC, &h, &m, &s);
		if (minuto!=m){
			minuto = m;
			char text[150];
			if (h<12){
				sprintf(text, "H%d:%dAM", h, m);
			}
			else{
				sprintf(text, "H%d:%dPM", h, m);
			}
			gfx_mono_draw_string(text, 0, 0, &sysfont);
		}
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
			rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
			
			flag_led0 = !flag_led0;
			
			if (flag_led0){
				pmc_enable_periph_clk(ID_TC1);
			}
			else{
				pmc_disable_periph_clk(ID_TC1);
			}
		
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}

/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter � meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrup�c�o no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup�c�o no TC canal 0 */
	/* Interrup��o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN | RTC_IER_SECEN);

}

void BUT_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_set_input(BUT_PIO, BUT_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(BUT_PIO, BUT_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIN_MASK, PIO_IT_FALL_EDGE, Button0_Handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
};

void BUT1_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
};

void BUT2_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT2_PIO_ID);
	pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, Button2_Handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 1);
};

void BUT3_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT3_PIO_ID);
	pio_set_input(BUT3_PIO, BUT3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(BUT3_PIO, BUT3_PIN_MASK);
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, PIO_IT_FALL_EDGE, Button3_Handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT3_PIO_ID);
	NVIC_SetPriority(BUT3_PIO_ID, 1);
};

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

void LED_init(int estado){
	pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIN_MASK, estado, 0, 0 );
};


int main (void)
{
    char text[150];
	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();
	LED_init(0);
	TC_init(TC0, ID_TC1, 1, 4);
	BUT_init();
	BUT1_init();
	BUT2_init();
	BUT3_init();
	RTC_init();
	
	
	
	
	//subescrevendo o despertador anterior
	rtc_set_date_alarm(RTC, 1, 2, 1, 2);
	rtc_set_time_alarm(RTC, 1, 2, 1, 6, 1, 2);
	
	flag_led0 = 0;
	libera_ier = 1;
	flag_main = 1;
	flag_menos = 0;
	flag_mais = 0;
	flag_desativa = 0;
	flag_tela = 1;
	
	pio_set(LED_PIO, LED_PIN_MASK);
	
	
	while(1) {
		if (flag_main){
			uint32_t h,m,s;
			rtc_get_time(RTC, &h, &m, &s);
			if (h<12){
				if (flag_tela){
					sprintf(text, "H%d:%dAM", h, m);
				}
				else{
					sprintf(text, "A%d:%dAM", h, alarmeminuto);
				}
				
				
			}
			else{
				if (flag_tela){
					sprintf(text, "H%d:%dPM", h, m);
				}
				else{
					sprintf(text, "A%d:%dAM", h, alarmeminuto);
				}

			}
				
			gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
			
			gfx_mono_draw_string(text, 0, 0, &sysfont);
			flag_main = 0;
		}
		if(flag_menos){
			char text[150];
			alarmeminuto-=1;
			rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
			rtc_set_time_alarm(RTC, 1, HOUR, 1, alarmeminuto, 1, SECOND);
			if (HOUR<12){
				sprintf(text, "A%d:%dAM", HOUR, alarmeminuto);
			}
			else{
				sprintf(text, "A%d:%dPM", HOUR, alarmeminuto);
			}
			
			gfx_mono_draw_string(text, 0, 0, &sysfont);
			flag_menos = 0;
			
		}
		if(flag_mais){
			char text[150];
			alarmeminuto+=1;
			rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
			rtc_set_time_alarm(RTC, 1, HOUR, 1, alarmeminuto, 1, SECOND);
			if (HOUR<12){
				sprintf(text, "A%d:%dAM", HOUR, alarmeminuto);
			}
			else{
				sprintf(text, "A%d:%dPM", HOUR, alarmeminuto);
			}
			gfx_mono_draw_string(text, 0, 0, &sysfont);
			flag_mais = 0;
		}
		if(flag_desativa){
			flag_led0 = 0;
			pio_set(LED_PIO, LED_PIN_MASK);
			
			pmc_disable_periph_clk(ID_TC1);
			//coloca um hor�rio que n�o v� despertar t�o j�...
			rtc_set_date_alarm(RTC, 1, 1, 1, 1);
			rtc_set_time_alarm(RTC, 1, 1, 1, 1, 1, 1);
	
			gfx_mono_draw_string("AOffline", 0, 0, &sysfont);
			flag_desativa = 0;
		}

	
	}
}
