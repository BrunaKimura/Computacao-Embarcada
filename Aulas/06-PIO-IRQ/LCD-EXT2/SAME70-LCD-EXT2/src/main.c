#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL
	
#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 19
#define BUT_PIO_PIN_MASK ( 1 << BUT_PIO_PIN)


#define BUT2_PIO PIOC
#define BUT2_PIO_ID 12
#define BUT2_PIO_PIN 31
#define BUT2_PIO_PIN_MASK ( 1 << BUT2_PIO_PIN)

struct ili9488_opt_t g_ili9488_display_opt;

/**
}
/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */

volatile but_flag = false;
volatile but2_flag = false;

void but_callBack(void){
	but_flag = true;
}

void but2_callBack(void){
	but2_flag = true;
}

void pisca_led(int freq){
	
	for (int i = 0; i <5; i++){
		pio_set(PIOC, LED_PIO_PIN_MASK);
		delay_ms(freq);
		// Coloca 0 no pino do LED
		pio_clear(PIOC, LED_PIO_PIN_MASK);
		delay_ms(freq);
	}
	pio_set(PIOC, LED_PIO_PIN_MASK);
}

void desenho(tempo){
	// array para escrita no LCD
	uint8_t stingLCD[256];
	float freq = 1.0/(tempo*0.001);
	/* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_draw_filled_rectangle(0, 280, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stingLCD, "Frequencia: %.3f Hz", freq);
	ili9488_draw_string(10, 300, stingLCD);
	
	sprintf(stingLCD, "Periodo: %d ms", tempo);
	ili9488_draw_string(10, 280, stingLCD);
	
}


int main(void)
{
	// array para escrita no LCD
	uint8_t stingLCD[256];
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	// Disativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);

    /* Inicializa e configura o LCD */
	configure_lcd();
	
	// Ativa  o periferico responsavel pelo controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	
	// Ativa o periferico responsavel pelo botao
	pmc_enable_periph_clk(BUT_PIO_ID);
	// Ativa o periferico responsavel pelo botao
	pmc_enable_periph_clk(BUT2_PIO_ID);
	
	// Inicializa PC8 como sa�da
	pio_configure(PIOC, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	
	// Configura pino como entrada
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	
	// Configura pino como entrada
	pio_configure(BUT2_PIO, PIO_INPUT, BUT2_PIO_PIN_MASK, PIO_PULLUP);
	
	
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_enable_interrupt(BUT2_PIO, BUT2_PIO_PIN_MASK);
	
	pio_handler_set(BUT_PIO, 
					BUT_PIO_ID, 
					BUT_PIO_PIN_MASK,
					PIO_IT_FALL_EDGE,
					but_callBack);
					
	pio_handler_set(BUT2_PIO, 
					BUT2_PIO_ID, 
					BUT2_PIO_PIN_MASK,
					PIO_IT_FALL_EDGE,
					but2_callBack);
					
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
	
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 0);
	
	pio_set(PIOC, LED_PIO_PIN_MASK);
	

    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stingLCD, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, stingLCD);
	
	int freq = 200;

	while (1) {
		//pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		
		
		
		if(but_flag){
			freq+=20;
			desenho(freq);
			
			but_flag = false;
		}
		
		else if(but2_flag){
			freq-=20;
			desenho(freq);
			
			but2_flag = false;
		}
	
				pisca_led(freq);

	}
	return 0;
}
