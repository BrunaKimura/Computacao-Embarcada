/**/
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO PIOC
#define LED_PIO_ID ID_PIOC
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << LED_PIO_PIN)

/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/
void but_callBack(void){
	int i = 0;
		
	while (i<5) {
		
		pio_clear(LED_PIO, LED_PIO_PIN_MASK);
		
		delay_ms(200);
		
		pio_set(LED_PIO, LED_PIO_PIN_MASK);
		
		delay_ms(200);  
			
		i++;                       
	}
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
	
}


/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	//board clock
	sysclk_init();
	
	//Desliga watchdog
	WDT->WDT_MR = WDT_MR_WDDIS;	

	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	
	pio_handler_set(BUT_PIO, 
					BUT_PIO_ID, 
					BUT_PIO_PIN_MASK,
					PIO_IT_RISI_EDGE,
					but_callBack);
	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	return 0;
}

