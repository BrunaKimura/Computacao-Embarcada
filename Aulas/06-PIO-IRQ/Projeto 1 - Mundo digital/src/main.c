/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK ( 1 << BUT_PIO_PIN)

/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

/************************************************************************/
/* Main                                                                 */
/************************************************************************/
volatile but_flag = false;

void but_callBack(void){
	but_flag = true;
}

void pisca_led(){
	for (int i = 0; i <5; i++){
		pio_set(PIOC, LED_PIO_PIN_MASK);
		delay_ms(200);
		// Coloca 0 no pino do LED
		pio_clear(PIOC, LED_PIO_PIN_MASK);
		delay_ms(200);
	}
	pio_set(PIOC, LED_PIO_PIN_MASK);
}




// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	// Initialize the board clock
	sysclk_init();
		
	// Disativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	// Ativa  o periferico responsavel pelo controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	
	// Ativa o periferico responsavel pelo botao
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	// Inicializa PC8 como sa�da
	pio_configure(PIOC, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	
	// Configura pino como entrada
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	
	
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	
	pio_handler_set(BUT_PIO, 
					BUT_PIO_ID, 
					BUT_PIO_PIN_MASK,
					PIO_IT_FALL_EDGE,
					but_callBack);
					
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 0);
	
	pio_set(PIOC, LED_PIO_PIN_MASK);
	
	
	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	while(1){
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		
		if(but_flag){
			pisca_led();
			
			but_flag = false;
		}
	}

		
	return 0;
}
