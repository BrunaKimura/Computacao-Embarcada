# Documentação do exercício 06 - PIO-IRQ

## Descrição
Este handout tinha como objeto explicar a utilização das interrupções, com os seguintes tópicos: callback, handler, NVIC, flag e ASF.
Após terminar o handout (entrega intermediária), era necessário alterar a frquencia que um LED pisca, através de botões externos a placa, um para aumentar a frequência e outro pra diminui-la. Além disso, era preiso exibir em um LCD a frequencia em que o LED estva piscando (entrega final).

## Materiais
O principal elemento do projeto foi a placa SAME70, junto com o OLED (utilizado apenas como um botão externo à placa) e jumpers. O LED utilizado foi o da própria placa.

## Diagrama

![alt text](Diagrama06.jpeg)

Como é possível ver pela imagem, o LCD é conectado pelos pinos da área EXT2. Já o OLED foi conectado da seguinte forma: os botões 1 e 2 foram conectados nos pinos PA19 e PC31 respectivamente. Os pinos do OLED que correspondem ao botão 1 e 2 são os pinos 9 e 3 respectivamente, o GND (pino 2 ou 19) e VCC (pino 20), também foram conectados na placa, sendo que a alimentação era de 3V.