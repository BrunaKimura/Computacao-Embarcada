/************************************************************************/
/* Includes                                                             */
/************************************************************************/

#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"
#include <assert.h>

/************************************************************************/
/* Defines                                                              */
/************************************************************************/

/** Header printf */
#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL
	

/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
/** 2^12 - 1                  */
#define MAX_DIGITAL     (4095UL)

/** The conversion data is done flag */
volatile bool is_conversion_done = false;

/** The conversion data value */
volatile uint32_t g_ul_value = 0;

/* Canal do sensor de temperatura */
#define AFEC_CHANNEL_TEMP_SENSOR 11

#define TASK_CON_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_CON_STACK_PRIORITY        (tskIDLE_PRIORITY)

/* Canal do potenciometro */
#define POT              AFEC0
#define POT_ID           ID_AFEC0
#define POT_CH           AFEC_CHANNEL_5 // Pin PB2
#define POT_CH_IR        AFEC_INTERRUPT_EOC_5
#define POT_THRESHOLD    50


struct ili9488_opt_t g_ili9488_display_opt;

QueueHandle_t QueueConvValue;
QueueHandle_t QueuePot;
/************************************************************************/
/* Callbacks / Handler                                                 */
/************************************************************************/

/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void){
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

/*
 * \brief AFEC interrupt callback function.
 */
static void AFEC_Temp_callback(void)
{
	uint32_t ul_value;
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	
	/* Unblock the task by releasing the semaphore. */
	//xSemaphoreGiveFromISR(SemaphoreConvIsDone, &xHigherPriorityTaskWoken );

	ul_value = afec_channel_get_value(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	xQueueSendFromISR(QueueConvValue, &ul_value, &xHigherPriorityTaskWoken);
}

static void AFEC_Pot_callback(void)
{
	uint32_t ul_value;
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	
	ul_value = afec_channel_get_value(AFEC0, POT_CH);
	xQueueSendFromISR(QueuePot, &ul_value, &xHigherPriorityTaskWoken);
}


/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/*
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


/** 
 * converte valor lido do ADC para temperatura em graus celsius
 * input : ADC reg value
 * output: Temperature in celsius
 */
static int32_t convert_adc_to_temp(int32_t ADC_value){
  
  int32_t ul_vol;
  int32_t ul_temp;
  
	ul_vol = ADC_value * VOLT_REF / MAX_DIGITAL;

  /*
   * According to datasheet, The output voltage VT = 0.72V at 27C
   * and the temperature slope dVT/dT = 2.33 mV/C
   */
  ul_temp = (ul_vol - 720)  * 100 / 233 + 27;
  return(ul_temp);
}

static int32_t convert_adc_to_pot(int32_t pot_value){
	
	return (1000*pot_value)/4067;
}

static void config_ADC_TEMP(void){
/************************************* 
   * Ativa e configura AFEC
   *************************************/  
  /* Ativa AFEC - 0 */
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_11,	AFEC_Temp_callback, 1); 
	//AFEC_Pot_callback
	afec_set_callback(AFEC0, POT_CH_IR,	AFEC_Pot_callback, 1);
	
	/*** Configuracao espec�fica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, 0x200);

/*** Configuracao espec�fica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg2;
	afec_ch_get_config_defaults(&afec_ch_cfg2);
	afec_ch_cfg2.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, POT_CH, &afec_ch_cfg2);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0, POT_CH, 0x200);
	
	/***  Configura sensor de temperatura ***/
	struct afec_temp_sensor_config afec_temp_sensor_cfg;

	afec_temp_sensor_get_config_defaults(&afec_temp_sensor_cfg);
	afec_temp_sensor_set_config(AFEC0, &afec_temp_sensor_cfg);

	/* Selecina canal e inicializa convers�o */  
	afec_channel_enable(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	afec_channel_enable(AFEC0, POT_CH);
	
	NVIC_SetPriority(ID_AFEC0, 5);
}

static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}


static void task_con(void *pvParameters){
	
	/* Block for 500ms. */
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
	
	
	for(;;){
		/* incializa convers�o ADC */
		afec_start_software_conversion(AFEC0);
		printf("task con start !!! \n");
	
		vTaskDelay(xDelay);
	}
	
}

static void task_ihm(void *pvParameters){
	
	uint32_t tempAtual;
	uint32_t tempPot;
	
	QueueConvValue = xQueueCreate( 10, sizeof(uint32_t) );
	QueuePot = xQueueCreate( 10, sizeof(uint32_t) );
	
	for (;;) {
		if(xQueueReceive( QueueConvValue, &tempAtual, ( TickType_t ) 100 ) == pdTRUE) {
			
			printf("Temp : %d \r\n", convert_adc_to_temp(tempAtual));
			
			/* Escreve na tela Computacao Embarcada 2018 */
			ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
			ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 340);
			ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
			 
			char stingLCD[128];
			
			sprintf(stingLCD, "temp: %d", convert_adc_to_temp(tempAtual));
			ili9488_draw_string(10, 300, stingLCD);
			
			if(xQueueReceive(QueuePot, &tempPot, ( TickType_t ) 100 ) == pdTRUE){
				char stingLCD[128];
			
				sprintf(stingLCD, "Potenciometro: %d", convert_adc_to_pot(tempPot));
				ili9488_draw_string(10, 320, stingLCD);
			}
			
			if (convert_adc_to_temp(tempAtual)<30){
				ili9488_set_foreground_color(COLOR_CONVERT(COLOR_GREEN));
				ili9488_draw_filled_circle(70, 280, 10);
			}
			else if(convert_adc_to_temp(tempAtual)>50){
				ili9488_set_foreground_color(COLOR_CONVERT(COLOR_RED));
				ili9488_draw_filled_circle(70, 280, 10);
			}
			else{
				ili9488_set_foreground_color(COLOR_CONVERT(COLOR_YELLOW));
				ili9488_draw_filled_circle(70, 280, 10);
			}
		}
	}
}


/************************************************************************/
/* Main                                                                 */
/************************************************************************/
/**
 * \brief Application entry point.
 *
 * \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	// array para escrita no LCD
	uint8_t stingLCD[256];
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	/* inicializa delay */
	delay_init(sysclk_get_cpu_hz());
	
	/* inicializa e configura adc */
	config_ADC_TEMP();
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);

    /* Inicializa e configura o LCD */
	configure_lcd();
	

    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stingLCD, "Bruna Kimura");
	ili9488_draw_string(10, 30, stingLCD);

	
	/* Create task to monitor processor activity */
	if (xTaskCreate(task_ihm, "ihm", TASK_CON_STACK_SIZE, NULL,
	TASK_CON_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Con task\r\n");
	}
	if (xTaskCreate(task_con, "Con", TASK_CON_STACK_SIZE, NULL,
	TASK_CON_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Con task\r\n");
	}
	
	
	
	/* Start the scheduler. */
	vTaskStartScheduler();


	return 0;
}
