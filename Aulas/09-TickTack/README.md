# Projeto 09 - Tick Tack

## Descriçãao

Neste projeto foram feitos 3 LED's piscarem em determinadas frequência e quando o botão correspondente ao LED era apertado os LED's paravam de piscar. Em paralelo a esse funcionamento, o LED piscava durante 1 minuto e no próximo minuto parava, assim alternando de minuto a minuto.

## Diagrama de blocos

![alt text](diagrama.jpeg)