Entregas Computação Embarcada


| Aula                     | Handout / Entrega Parcial | Entrega Final|   Pesquisa   |
|:------------------------:|:-------------------------:|:------------:|:------------:|
| 01 - Introdução          | [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/01-Introducao)|              |              |
| 02 - Mutirão C           | [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/02-MutiraoC/SAME70-Matrizes)|              |  [APS](https://gitlab.com/BrunaKimura/Computacao-Embarcada/blob/master/Aps1.pdf)            |
| 03 - IOs Digitais        | [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/03-IO-Digital)| [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/03-IO-Digital/SAME70)     | [&#x1F4AC;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/blob/master/Estudo-Perifericos.pdf)    |
| 04 - Projeto A           |                           |              |              |
| 05 - PIO - In/Out        | [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/04-PIO/SAME70)| [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/04-PIO/SAME70)     | [&#x1F4AC;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/blob/master/Estudo-Perifericos.pdf)   |
| 06 - Projeto A           |                           |              |              |
| 07 - PIO - IRQ           | [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/06-PIO-IRQ)| [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/06-PIO-IRQ/Projeto%201%20-%20Mundo%20digital)| [&#x1F4AC;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/07-PIO-IRQ)    |
| 08 - Projeto Idealização |                           |              |              |
| 09 - TickTack            | [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/09-TickTack)| [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/09-TickTack/SAME70-TickTack/SAME70-TC-RTC)     | [&#x1F4AC;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/blob/master/Aulas/09-TickTack/Pesquisa%2009.pdf)    |
| 11 - RTOS     &#x1F552;  | [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/11-RTOS)| [&#x2714;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/11-RTOS/FREERTOS_SAM_EXAMPLE1/FREERTOS_SAM_EXAMPLE1)     | [&#x1F4AC;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/blob/master/Aulas/11-RTOS/Pesquisa%2011.pdf)    |
| 12 - RTOS - UART         | [&#x1F4AC;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/12-RTOS-UART)| [&#x1F4AC;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/12-RTOS-UART/RTOS-UART-Demo)     |   |
| 13 - RTOS - ADC          || [&#x270F;](https://gitlab.com/BrunaKimura/Computacao-Embarcada/tree/master/Aulas/13%20-%20RTOS%20-%20ADC)|  |

Legenda:
  -  &#x2714; : Feito e corrigido;
  -  &#x1F4AC; : Feito mas não corrigido;
  -  &#x1F552; : Atrasado;
  -  &#x270F; : Fazendo;
  -  &#x2716; : Não feito.
    
    
As entregas parciais/finais estão descritas no handout da aula.